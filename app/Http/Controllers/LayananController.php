<?php

namespace App\Http\Controllers;

use App\Layanan;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class LayananController extends Controller
{
	/**
	* @SWG\Get(
	*     path="/api/layanan/{id}",
	*     description="Mengembalikan daftar layanan",
	*     operationId="api.layanan.index",
	*     produces={"application/json"},
	*     tags={"layanan"},
	*			@SWG\Parameter(
	*				name="id",
	*				in="path",
	*				description="ID layanan yang ingin didapat",
	*				required=false,
	*				type="integer",
	*				format="int32"
	*			),
	*     @SWG\Response(
	*       response=200,
	*       description="Daftar layanan"
	*     ),
	*     @SWG\Response(
	*       response=404,
	*       description="Not found",
	*     )
	* )
	*/
	public function index($id = null) {
		if ($id == null) {
			$layanan = Layanan::orderBy('id', 'asc')->get();
		}
		else {
			$layanan = Layanan::orderBy('id', 'asc')->where('id', $id)->first();
		}
		return response()->json(array('data' => $layanan));
	}

	/**
	* @SWG\Post(
	*     path="/api/layanan",
	*     description="Membuat layanan baru",
	*     operationId="api.layanan.store",
	*			consumes={"application/json", "application/xml"},
	*     produces={"string"},
	*     tags={"layanan"},
	*     @SWG\Response(
	*       response=200,
	*       description="Layanan baru telah dibuat"
	*     ),
	*     @SWG\Response(
	*       response=404,
	*       description="Not found",
	*     )
	* )
	*/
	public function store(Request $request) {
		$layanan = new Layanan;

		$layanan->nama = $request->input('nama');
		$layanan->keterangan = $request->input('keterangan');
		$layanan->harga = $request->input('harga');
		$layanan->save();

		return response()->json(['response' => 'Layanan baru telah dibuat dengan id: ' . $layanan->id]);
	}

	/**
	* @SWG\Post(
	*     path="/api/layanan/{id}",
	*     description="Mengupdate layanan",
	*     operationId="api.layanan.update",
	*     produces={"application/json"},
	*     tags={"layanan"},
	*			@SWG\Parameter(
	*				name="id",
	*				in="path",
	*				description="ID layanan yang ingin diupdate",
	*				required=true,
	*				type="integer",
	*				format="int32"
	*			),
	*     @SWG\Response(
	*       response=200,
	*       description="Layanan berhasil dihapus"
	*     ),
	*     @SWG\Response(
	*       response=404,
	*       description="Not found",
	*     )
	* )
	*/
  public function update(Request $request, $id) {
    $layanan = Layanan::find($id);

    if ($request->input('nama') != null)
    	$layanan->nama = $request->input('nama');
    if ($request->input('keterangan') != null)
    	$layanan->keterangan = $request->input('keterangan');
    if ($request->input('harga') != null)
    	$layanan->harga = $request->input('harga');
    $layanan->save();

    return response()->json(['response' => 'Berhasil update layanan #' . $layanan->id]);
  }

	/**
	* @SWG\Delete(
	*     path="/api/layanan/{id}",
	*     description="Menghapus layanan",
	*     operationId="api.layanan.destroy",
	*     produces={"application/json"},
	*     tags={"layanan"},
	*			@SWG\Parameter(
	*				name="id",
	*				in="path",
	*				description="ID layanan yang ingin dihapus",
	*				required=true,
	*				type="integer",
	*				format="int32"
	*			),
	*     @SWG\Response(
	*       response=200,
	*       description="Layanan berhasil dihapus"
	*     ),
	*     @SWG\Response(
	*       response=404,
	*       description="Not found",
	*     )
	* )
	*/
  public function destroy($id) {
    $layanan = Layanan::find($id);
    $layanan->delete();
    return response()->json(['response' => 'Data layanan #' . $id . ' berhasil dihapus']);
  }
}