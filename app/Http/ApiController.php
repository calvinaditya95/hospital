<?php
namespace App\Http\Controllers;
/**
 * Class ApiController
 *
 * @package App\Http\Controllers
 *
 * @SWG\Swagger(
 *     basePath="",
 *     host="hospital.dev",
 *     schemes={"http"},
 *     @SWG\Info(
 *         version="1.0",
 *         title="Hospital Management System API",
 *         @SWG\Contact(name="Calvin Aditya Jonathan", email="13513077@std.stei.itb.ac.id"),
 *     ),
 *     @SWG\Definition(
 *         definition="Error",
 *         required={"code", "message"},
 *         @SWG\Property(
 *             property="code",
 *             type="integer",
 *             format="int32"
 *         ),
 *         @SWG\Property(
 *             property="message",
 *             type="string"
 *         )
 *     )
 * )
 */
class ApiController extends Controller
{
}