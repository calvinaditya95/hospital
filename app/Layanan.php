<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Layanan extends Model
{
	protected $table = 'layanan';
	protected $fillable = array('id', 'nama', 'keterangan','harga');
}
